<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Plastico</title>

{{--    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/product/">--}}

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <form class="form-inline buscador">
        <button class="btn btn-outline-success my-2 my-sm-0 mr-1" type="submit"><i class="fa far-search"></i></button>
        <input class="form-control mr-sm-2" type="search" placeholder="Estoy buscando..." aria-label="Search">
    </form>

    <div class="top-right links mr-5">
        <div class="row justify-content-start">
        @if (Route::has('login'))
            <div class="col-3 row-cols-1">
                <img class="img-avatar img-avatar96 p-0" src="{{asset('avatar1.jpg')}}" alt="">
            </div>
            <div class="top-right links col-7 ">
                @auth
                    <a href="{{ url('/home') }}">Home</a>
                @else

                    <h6 class="text-primary" >Acceso de Clientes</h6>
                    <a class="iniciolink" href="{{ route('login') }}">Iniciar sesión</a>

{{--                    @if (Route::has('register'))--}}
{{--                        <a href="{{ route('register') }}">Register</a>--}}
{{--                    @endif--}}
                @endauth
            </div>
        @endif

        </div>
    </div>
</nav>
<nav class="site-header  sticky-top py-1">
    <div class="container d-flex flex-column flex-md-row justify-content-between">

        <a class="py-2 d-none d-md-inline-block" href="#">INICIO</a>
        <a class="py-2 d-none d-md-inline-block" href="#">EMPRESA</a>
        <a class="py-2 d-none d-md-inline-block" href="#">SOLICITAR PRESUUESTO</a>
        <a class="py-2 d-none d-md-inline-block" href="#">CALIDAD</a>
        <a class="py-2 d-none d-md-inline-block" href="#">PRODUCTOS</a>
        <a class="py-2 d-none d-md-inline-block" href="#">CLIENTES</a>
    </div>
</nav>
<div class="container-fluid">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100 h-75" src="{{asset('bot.jfif')}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 h-75" src="{{asset('bot2.jfif')}}" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 h-75" src="{{asset('bot3.jfif')}}" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="row justify-content-around mt-5">
        <div class="col-4">
           <h3 class="text-primary">One of two columns One of two columnsOne of two columnsOne of two columns</h3>
            <p class="text-secondary"> of two columns One of tw</p>
        </div>
        <div class="col-4">
            <p class="text-secondary"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
            <ul class="list-group text-capitalize text-primary">
                <li class="list"><h2>Cras justo odio</h2></li>
                <li class="list"><h2>Cras justo odiobus ac facilisis in</h2></li>
                <li class="list"><h2>Cras justo Morbi leo risus</h2></li>
                <li class="list"><h2>Cras justo oa ac consectetur ac</h2></li>

            </ul>

        </div>

    </div>


    <!-- Page Content -->
    <div class="w-100 h-auto mt-5 mb-3" style="background-color: #eee">

        <div class="row mb-5">
            <div class="col-xl-8 mt-5 mb-5">
                <!-- Story -->
                <div class="block ">
                    <div class="block-content ml-5 ">
                        <div class="row items-push d-flex justify-content-center ml-5 ">
                            <div class="col-md-4col-lg-5 ml-5">
                                <a href="be_pages_blog_story.html">
                                    <img class="img-fluid" src="{{asset('bot.jfif')}}" alt="">
                                </a>
                            </div>
                            <div class="col-md-8 col-lg-7">
                                <h4 class="h3 mb-1">
                                    <a class="text-primary-dark" href="be_pages_blog_story.html">Top 10 Destinations</a>
                                </h4>
                                <div class="font-size-sm mb-3">
                                    <a href="be_pages_generic_profile.html">Laura Carr</a> on July 16, 2019 · <em class="text-muted">10 min</em>
                                </div>
                                <p class="font-size-sm">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh..
                                </p>
                                <a class="btn btn-sm btn-light" href="be_pages_blog_story.html">Continue Reading..</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Story -->

                <div class="block mt-5">
                    <div class="block-content ml-5 ">
                        <div class="row items-push d-flex justify-content-center ml-5 ">

                            <div class="col-md-8 col-lg-7">
                                <h4 class="h3 mb-1">
                                    <a class="text-primary-dark" href="be_pages_blog_story.html">Top 10 Destinations</a>
                                </h4>
                                <div class="font-size-sm mb-3">
                                    <a href="be_pages_generic_profile.html">Laura Carr</a> on July 16, 2019 · <em class="text-muted">10 min</em>
                                </div>
                                <p class="font-size-sm">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh..
                                </p>
                                <a class="btn btn-sm btn-light" href="be_pages_blog_story.html">Continue Reading..</a>
                            </div>
                            <div class="col-md-4col-lg-5 ml-5">
                                <a href="be_pages_blog_story.html">
                                    <img class="img-fluid" src="{{asset('bot.jfif')}}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
     </div>
    <!-- More Stories -->
    <h3 class="text-info m-5">Productos destacados <hr></h3>
    <div class="content content-boxed ">
        <!-- Section Content -->
        <div class="row py-5 m-5">
            <div class="col-md-3">
                <img class="img-fluid" src="{{asset('bot4.jfif')}}" alt="">
                    <div class="block-content block-content-full font-size-sm">
                        <span class="text-secondary">cod 001</span>
                        <h5 class="text-info">botellas</h5>
                    </div>

            </div>
            <div class="col-md-3">

                <img class="img-fluid" src="{{asset('bot4.jfif')}}" alt="">
                    <div class="block-content block-content-full font-size-sm">
                        <span class="text-secondary">cod 001</span>
                        <h5 class="text-info">botellas</h5>
                    </div>

            </div>
            <div class="col-md-3">

                <img class="img-fluid" src="{{asset('bot4.jfif')}}" alt="">
                    <div class="block-content block-content-full font-size-sm">
                        <span class="text-secondary">cod 001</span>
                        <h5 class="text-info">botellas</h5>
                    </div>
            </div>
            <div class="col-md-3">

                <img class="img-fluid" src="{{asset('bot4.jfif')}}" alt="">
                    <div class="block-content block-content-full font-size-sm">
                        <span class="text-secondary">cod 001</span>
                        <h5 class="text-info">botellas</h5>
                    </div>

            </div>

        </div>
        <!-- END Section Content -->
    </div>
    <!-- END More Stories -->
    <div class="row justify-content-around mt-5">
        <div class="col-4">
            <h3 class="text-info">Confian en nosotros</h3>
            <div id="carouselExampleIndicators" class="carousel slide " data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-75 h-50" src="{{asset('sonso.jfif')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-75 h-25" src="{{asset('sonso.jfif')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-75 h-25" src="{{asset('sonso.jfif')}}" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>


    </div>
</div>
{{--<footer class="container py-5">--}}
    <!-- Footer -->
    <footer id="page-footer" class="m-3 h-25">
        <div class="content py-3 bg-body-dark bg-dark ">
            <div class="row py-5 ml-5" >
                <div class="col-md-2">
                    <h6 class="text-info">Contacto</h6>
                    <div class="block-content block-content-full font-size-sm">
                        <p class="text-light">prueba</p>
                        <p class="text-light">prueba</p>
                    </div>

                </div>
                <div class="col-md-2">

                    <h6 class="text-info">Contacto</h6>
                    <div class="block-content block-content-full font-size-sm">
                        <p class="text-light">prueba</p>
                        <p class="text-light">prueba</p>
                    </div>

                </div>
                <div class="col-md-2">

{{--                    <h6 class="text-info">Contacto</h6>--}}
                    <div class="block-content block-content-full font-size-sm">
{{--                        <p class="text-light">prueba</p>--}}
{{--                        <p class="text-light">prueba</p>--}}
                    </div>
                </div>
                <div class="col-md-2">

                    <h6 class="text-info">Contacto</h6>
                    <div class="block-content block-content-full font-size-sm">
                        <p class="text-light">prueba</p>
                        <p class="text-light">prueba</p>

                    </div>

                </div>

                <div class="col-md-2">
                    <h6 class="text-info">Contacto</h6>
                    <div class="block-content block-content-full font-size-sm">
                        <p class="text-light">prueba</p>
                        <p class="text-light">prueba</p>
                    </div>

                </div>
                <div class="col-md-2">
                   <h6 class="text-info">Contacto</h6>
                    <div class="block-content block-content-full font-size-sm">
                        <p class="text-light">prueba</p>
                        <p class="text-light">prueba</p>


                    </div>
                </div>

            </div>
            <hr class=" bg-white m-3 ">
            <div class=" bg-dark mb-5"><p class="text-secondary mr-5 "  style="float: right !important;" >plas</p></div>
        </div>

{{--    </footer>--}}
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js" ><\/script>')</script><script src="{{asset('js/bootstrap.bundle.min.js')}}" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script></body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>

