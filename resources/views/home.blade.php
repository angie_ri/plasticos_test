@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Panel de Administración</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Control de Accesos
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">

                                    <div class="card-body">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr class="table-secondary">
                                                <th scope="col">#</th>
                                                <th scope="col">Usuario</th>
                                                <th scope="col">Estado</th>
                                                <th scope="col">IP</th>
                                                <th scope="col">Fecha</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(isset($control_accesos))
                                                @foreach($control_accesos as $ca)
                                                    <tr>
                                                        <th scope="row">{{$ca->id_control}}</th>
                                                        <td>{{$ca->user->name}}</td>
                                                        <td>{{$ca->ip}}</td>
                                                        <td>{{$ca->estado}}</td>
                                                        <td>{{$ca->created_at}}</td>
                                                    </tr>

                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                        @if(isset($control_accesos))
                                             <div class="ml-5"> {{ $control_accesos->links() }}</div>
                                         @endif
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Usuarios
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr class="table-secondary">
                                                <th scope="col">#</th>
                                                <th scope="col">Usuario</th>
                                                <th scope="col">Email</th>
                                                <th scope="col">Estado</th>
                                                <th scope="col">Ingreso</th>
                                                <th scope="col">Editar</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(isset($users))
                                                @foreach($users as $us)
                                                    <tr>
                                                        <th scope="row">{{$us->id}}</th>
                                                        <td>{{$us->name}}</td>
                                                        <td>{{$us->email}}</td>
                                                        <td>{{$us->statu == 1? "Activo": "Desactivo" }}</td>
                                                        <td>{{$us->created_at}}</td>
                                                        <td><button type="button" class="btn-outline-success btn" data-toggle="modal" data-target="#modalEditar" data-user="{{$us->id}}" onclick="editar(this)">Editar</button>
                                                              </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        @if(isset($users))
                                            <div class="ml-5"> {{ $users->links() }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formEditar">
                    @csrf
                    <input type="hidden" class="form-control" id="idUser" name="user">

                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputName" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Estado</label>
                        <div class="col-sm-10">
                            <select class="custom-select" id="inputEstado" name="estado">
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" onclick="modificar()">Guardar</button>
            </div>
        </div>
    </div>

</div>
@endsection
<script>

    function modificar(){

        console.log($('#formEditar').serialize());
        $.ajax({
            url: "{{url('/')}}/guardar_user",
            data:$('#formEditar').serialize(),
            type: 'post',
            // dataType: 'json',
            success: function (data) {
                console.log(data);
                $("#modalEditar").modal('hide');
            },
            error: function() {
                // console.log("No se ha podido obtener la información");
            }
        });
    }
    function editar(b) {
        // console.log($(b).data('user'));
        var user_id = $(b).data('user');
        $.ajax({
            url: "{{url('/') }}/editar/id/"+user_id,
            type : 'GET',
            dataType : 'json',
            success: function(respuesta) {
                // console.log(respuesta);

                $('#idUser').val(user_id);
                $('#staticEmail').val(respuesta.email);
                $('#inputName').val(respuesta.name);
                var otro_statu =respuesta.statu == 1?0:1;
                var statu = respuesta.statu == 1?"Activo":"Desactivo";
                var statu2 = otro_statu == 0?"Desactivo":"Activo";
                // console.log(otro_statu,statu2);
                $('#inputEstado').append('<option value="'+respuesta.statu+'">'+statu+'</option>' +
                    '<option value="'+otro_statu+'">'+statu2+'</option>');
                // $('#modalEditar').html(respuesta);
                // // $('#modalEditar').modal({backdrop: 'static', keyboard: false})
                // $('#modalEditar').modal('show');
            },
            error: function() {
                // console.log("No se ha podido obtener la información");
            },
        });

    }
</script>
