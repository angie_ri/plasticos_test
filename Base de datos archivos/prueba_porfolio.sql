-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-02-2020 a las 01:16:44
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba_porfolio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `control_accesos`
--

CREATE TABLE `control_accesos` (
  `id_control` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `control_accesos`
--

INSERT INTO `control_accesos` (`id_control`, `id_user`, `ip`, `estado`, `updated_at`, `created_at`) VALUES
(1, 1, '127.0.0.1', 1, '2020-02-16 03:09:59', '2020-02-16 03:09:59'),
(2, 1, '127.0.0.1', 0, '2020-02-16 04:08:32', '2020-02-16 04:08:32'),
(3, 1, '127.0.0.1', 0, '2020-02-16 04:58:03', '2020-02-16 04:58:03'),
(4, 1, '127.0.0.1', 0, '2020-02-16 05:00:51', '2020-02-16 05:00:51'),
(5, 1, '127.0.0.1', 0, '2020-02-16 05:01:28', '2020-02-16 05:01:28'),
(6, 1, '127.0.0.1', 0, '2020-02-16 05:02:31', '2020-02-16 05:02:31'),
(7, 1, '127.0.0.1', 0, '2020-02-16 05:03:20', '2020-02-16 05:03:20'),
(8, 1, '127.0.0.1', 0, '2020-02-16 05:04:10', '2020-02-16 05:04:10'),
(9, 1, '127.0.0.1', 0, '2020-02-16 05:04:40', '2020-02-16 05:04:40'),
(10, 1, '127.0.0.1', 0, '2020-02-16 05:05:26', '2020-02-16 05:05:26'),
(11, 1, '127.0.0.1', 1, '2020-02-16 05:06:35', '2020-02-16 05:06:35'),
(12, 1, '127.0.0.1', 1, '2020-02-16 05:15:09', '2020-02-16 05:15:09'),
(13, 1, '127.0.0.1', 1, '2020-02-16 05:15:41', '2020-02-16 05:15:41'),
(14, 1, '127.0.0.1', 0, '2020-02-16 05:16:27', '2020-02-16 05:16:27'),
(15, 1, '127.0.0.1', 0, '2020-02-16 05:17:30', '2020-02-16 05:17:30'),
(16, 1, '127.0.0.1', 0, '2020-02-16 05:18:10', '2020-02-16 05:18:10'),
(17, 1, '127.0.0.1', 0, '2020-02-16 05:20:31', '2020-02-16 05:20:31'),
(18, 1, '127.0.0.1', 0, '2020-02-16 05:20:41', '2020-02-16 05:20:41'),
(19, 1, '127.0.0.1', 1, '2020-02-16 05:24:12', '2020-02-16 05:24:12'),
(20, 1, '127.0.0.1', 0, '2020-02-16 05:24:47', '2020-02-16 05:24:47'),
(21, 1, '127.0.0.1', 0, '2020-02-16 05:42:40', '2020-02-16 05:42:40'),
(22, 1, '127.0.0.1', 0, '2020-02-16 05:43:57', '2020-02-16 05:43:57'),
(23, 1, '127.0.0.1', 0, '2020-02-16 05:45:12', '2020-02-16 05:45:12'),
(24, 1, '127.0.0.1', 0, '2020-02-16 05:45:25', '2020-02-16 05:45:25'),
(25, 1, '127.0.0.1', 0, '2020-02-16 05:45:59', '2020-02-16 05:45:59'),
(26, 1, '127.0.0.1', 0, '2020-02-16 05:47:56', '2020-02-16 05:47:56'),
(27, 1, '127.0.0.1', 0, '2020-02-16 05:48:27', '2020-02-16 05:48:27'),
(28, 1, '127.0.0.1', 0, '2020-02-16 05:49:02', '2020-02-16 05:49:02'),
(29, 1, '127.0.0.1', 1, '2020-02-16 05:49:35', '2020-02-16 05:49:35'),
(30, 1, '127.0.0.1', 0, '2020-02-16 05:52:25', '2020-02-16 05:52:25'),
(31, 1, '127.0.0.1', 1, '2020-02-16 05:52:29', '2020-02-16 05:52:29'),
(32, 1, '127.0.0.1', 1, '2020-02-16 23:35:28', '2020-02-16 23:35:28'),
(33, 1, '127.0.0.1', 0, '2020-02-17 03:03:31', '2020-02-17 03:03:31'),
(34, 1, '127.0.0.1', 1, '2020-02-17 03:03:37', '2020-02-17 03:03:37'),
(35, 1, '127.0.0.1', 1, '2020-02-19 03:14:09', '2020-02-19 03:14:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statu` int(11) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `statu`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$Shu4z5V3UOsK8V0V/Pt4lemYb4zpvHo8WxDDJSKLcnmHzrPKXOIuW', 0, 'q4hRYqrSkyCVCAfP6TQOyCfb2ZnRzufnhvE1lKtVo7LxNEgK2E1TSZfVzXdJ', '2020-02-15 21:14:58', '2020-02-17 02:59:59');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `control_accesos`
--
ALTER TABLE `control_accesos`
  ADD PRIMARY KEY (`id_control`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `control_accesos`
--
ALTER TABLE `control_accesos`
  MODIFY `id_control` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
