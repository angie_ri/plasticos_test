<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControlAcceso extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'control_accesos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'ip', 'estado','fecha',
    ];
    public function user()
    {
        return $this->hasOne('App\User','id','id_user');
    }
}
