<?php

namespace App\Http\Controllers\Auth;

use App\ControlAcceso;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function  login(Request $request)
    {
        $credentials = $request->only( 'email','password');

        $ip =\Request::getClientIp(true);
        if (Auth::attempt($credentials)) {
            ControlAcceso::create([
                'id_user' => Auth::id(),
                'ip' => $ip,
            ]);

                return redirect()->intended('home');

        }else{

            $user =User::where('email',$credentials['email'])->first();
            if($user->id){
                ControlAcceso::create([
                    'id_user' => $user->id,
                    'ip' => $ip,
                    'estado'=> 0,
                ]);
            }
            return redirect()->back()->withErrors(['Ingrese contraseña correcta.']);

        }


    }
}
