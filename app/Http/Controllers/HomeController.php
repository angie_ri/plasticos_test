<?php

namespace App\Http\Controllers;

use App\ControlAcceso;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $users = User::paginate(10);
        $control_accesos = ControlAcceso::paginate(10);
        return view('home',compact('users','control_accesos'));
    }

    public  function editar($id)
    {
        if($id){

            $user = User::find($id);
            return response()->json($user,200);
        }

    }
    public  function guardar(Request $request)
    {
        if($request->user){

            $user = User::find($request->user);
            $user->name = $request->name;
            $user->statu = $request->estado;
            $user->updated_at = Carbon::now();
            $user->save();
            return response()->json("bien",200);
        }

    }

}
